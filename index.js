const express = require("express");
const http = require("http");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const dishRouter = require("./routes/dishRouter");
const promotionRouter = require("./routes/promotionRouter");
const leaderRouter = require("./routes/leaderRouter");
const hostname = "localhost";
const port = "3000";

const app = express();
app.use(morgan("dev"));
app.use(bodyParser.json());

// app.get("/dishes/:dishId", (req, res) => {
//   res.end("Will send dishe " + req.params.dishId + " to you");
// });

// app.post("/dishes/:dishId", (req, res) => {
//   res.statusCode = 403;
//   res.end("Post isnt supported");
// });

// app.put("/dishes/:dishId", (req, res) => {
//   res.write("Updaing the dish " + req.params.dishId);
//   res.end(
//     "\nwill update the dish " +
//       req.body.name +
//       " with details: " +
//       req.body.description
//   );
// });

// app.delete("/dishes/:dishId", (req, res) => {
//   res.end("Will delete the dishe with id " + req.params.dishId);
// });

app.use("/dishes", dishRouter);
app.use("/promotions", promotionRouter);
app.use("/leaders", leaderRouter);
app.use(express.static(__dirname + "/public")); //server to use static file

app.use((req, res, next) => {
  res.status = 200;
  res.setHeader("Content-Type", "text/html");
  res.end("<html><h1>This is an express server</h1></html>");
});

const server = http.createServer(app);
server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}/${port}`);
});
