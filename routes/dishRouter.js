const express = require("express");
const bodyParser = require("body-parser");

const dishRouter = express.Router();

dishRouter.use(bodyParser.json());

dishRouter
  .route("/")
  .all((req, res, next) => {
    res.statusCode = 200;
    res.setHeader("Content-Type", "text/plain");
    next();
  })
  .get((req, res, next) => {
    res.end("Will send all the dishes to you");
  })
  .post((req, res, next) => {
    res.end(
      "Will add the dish: " +
        req.body.name +
        "\nWill add the dish: " +
        req.body.description
    );
  })
  .put((req, res, next) => {
    res.statusCode = 403;
    res.end("Put isnt supported");
  })
  .delete((req, res, next) => {
    res.end("Will delete all the dishes");
  });

dishRouter
  .route("/:dishId")
  .all((req, res, next) => {
    res.statusCode = 200;
    res.setHeader("Content-Type", "text/plain");
    next();
  })
  .get((req, res, next) => {
    res.end("Will send dishe " + req.params.dishId + " to you");
  })

  .post((req, res, next) => {
    res.statusCode = 403;
    res.end("Post isnt supported");
  })

  .put((req, res, next) => {
    res.write("Updaing the dish " + req.params.dishId);
    res.end(
      "\nwill update the dish " +
        req.body.name +
        " with details: " +
        req.body.description
    );
  })

  .delete((req, res, next) => {
    res.end("Will delete the dishe with id " + req.params.dishId);
  });

module.exports = dishRouter;
