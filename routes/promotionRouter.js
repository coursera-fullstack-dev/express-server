const express = require("express");
const bodyParser = require("body-parser");

const promotionRouter = express.Router();

promotionRouter.use(bodyParser.json());

promotionRouter
  .route("/")
  .all((req, res, next) => {
    res.statusCode = 200;
    res.setHeader("Content-Type", "text/plain");
    next();
  })
  .get((req, res, next) => {
    res.end("Will send all the promotions to you");
  })
  .post((req, res, next) => {
    res.end(
      "Will add the promotion: " +
        req.body.name +
        "\nWill add the description: " +
        req.body.description
    );
  })
  .put((req, res, next) => {
    res.statusCode = 403;
    res.end("Put isnt supported");
  })
  .delete((req, res, next) => {
    res.end("Will delete all the promotions");
  });

promotionRouter
  .route("/:promoId")
  .all((req, res, next) => {
    res.statusCode = 200;
    res.setHeader("Content-Type", "text/plain");
    next();
  })
  .get((req, res, next) => {
    res.end("Will send promo " + req.params.promoId + " to you");
  })

  .post((req, res, next) => {
    res.statusCode = 403;
    res.end("Post isnt supported");
  })

  .put((req, res, next) => {
    res.write("Updaing the promo " + req.params.promoId);
    res.end(
      "\nwill update the promo " +
        req.body.name +
        " with details: " +
        req.body.description
    );
  })

  .delete((req, res, next) => {
    res.end("Will delete the promo with id " + req.params.promoId);
  });

module.exports = promotionRouter;
